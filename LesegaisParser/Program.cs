﻿using System;
using System.Collections.Generic;
using LesegaisParser;

namespace LesegaisParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new ParserService(new Request("https://www.lesegais.ru/open-area/graphql"));

            var size = 20;
            var count = parser.GetReportWoodDealCount(size, 0);


            var number = Math.Ceiling((decimal)count / size);

            var listReportWoodDealAll = new List<ReportWoodDeal>();

            for (int i = 0; i < number; i++)
            {
                var tempList = parser.ListReportWoodDeal(size, i);
                listReportWoodDealAll.AddRange(tempList);
                tempList.ForEach(x => 
                {
                    var info = string.Format("{0}, {1}, {2}, {3}, {4}, {5}", x.buyerName, x.buyerInn, x.dealDate, x.dealNumber, x.sellerInn, x.sellerName);
                    Console.WriteLine(info);
                });
            }

            Console.ReadKey();


        }
    }
}
