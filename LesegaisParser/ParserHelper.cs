﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LesegaisParser
{
    public class PostDataBytesQuery
    {
        public string OperationName { get; set; }
        public string Query { get; set; }
        public int Size { get; set; }
        public int Number { get; set; }
        public object Orders { get; set; }
    }

    public static class ParserHelper
    {
        public static byte[] GetPostDataBytes(PostDataBytesQuery query)
        {
            var root = new Root()
            {
                operationName = query.OperationName,
                query = query.Query,
                variables = new Variables()
                {
                    size = query.Size,
                    number = query.Number,
                    filter = null,
                    orders = query.Orders,
                }
            };

            var json = JsonConvert.SerializeObject(root);
            byte[] byteArray = Encoding.UTF8.GetBytes(json);

            return byteArray;
        }
    }
}
