﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LesegaisParser
{
    public class Request
    {
        private string _url;

        public Request(string url)
        {
            _url = url;
        }

        public string PostRequest(byte[] postData)
        {
            var request = (HttpWebRequest)WebRequest.Create(_url);

            request.Method = "POST";
            request.Accept = "*/*";
            request.Host = "www.lesegais.ru";
            request.Referer = "https://www.lesegais.ru/open-area/deal";
            request.ContentType = "application/json";
            request.ContentLength = postData.Length;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate, br");
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.9");

            using (var reqStream = request.GetRequestStream())
            {
                reqStream.Write(postData, 0, postData.Length);
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            return new StreamReader(stream).ReadToEnd();
                        }
                    }
                }
            }
            return "";
        }
    }
}
