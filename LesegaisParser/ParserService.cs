﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LesegaisParser
{

    public static class JsonIgnore
    {
        public static int OrderIgnore = 1;
    }

    public class Variables
    {
        public int size { get; set; }
        public int number { get; set; }
        public object filter { get; set; }
        public object orders { get; set; }
        public bool ShouldSerializeorders()
        {
            return (orders == null);
        }


    }
    public class Root
    {
        public string query { get; set; }
        public Variables variables { get; set; }
        public string operationName { get; set; }
    }

    public class SearchReportWoodDeal
    {
        public int total { get; set; }
        public int number { get; set; }
        public int size { get; set; }
        public double overallBuyerVolume { get; set; }
        public double overallSellerVolume { get; set; }
        public string __typename { get; set; }
    }

    public class Data
    {
        public SearchReportWoodDeal searchReportWoodDeal { get; set; }
    }

    public class Root2
    {
        public Data data { get; set; }
    }

    public class ParserService
    {
        Request _request;

        public ParserService(Request request)
        {
            _request = request;
        }

        public int GetReportWoodDealCount(int size, int number)
        {
            var postDataBytes = ParserHelper.GetPostDataBytes(new PostDataBytesQuery
            {
                OperationName = "SearchReportWoodDealCount",
                Query = "query SearchReportWoodDealCount($size: Int!, $number: Int!, $filter: Filter, $orders: [Order!]) { searchReportWoodDeal(filter: $filter, pageable: {number: $number, size: $size}, orders: $orders) { total number size overallBuyerVolume overallSellerVolume __typename } }",
                Size = size,
                Number = number,
                Orders = JsonIgnore.OrderIgnore,
            });

            if (_request != null)
            {
                var response = _request.PostRequest(postDataBytes);
                var searchReportWoodDeal = JsonConvert.DeserializeObject<Root2>(response);
                return searchReportWoodDeal.data.searchReportWoodDeal.total;
            }

            return 1;
        }

        public List<ReportWoodDeal> ListReportWoodDeal(int size, int number)
        {
            var result = new List<ReportWoodDeal>();

            var postDataBytes = ParserHelper.GetPostDataBytes(new PostDataBytesQuery
            {
                OperationName = "SearchReportWoodDeal",
                Query = "query SearchReportWoodDeal($size: Int!, $number: Int!, $filter: Filter, $orders: [Order!]) { searchReportWoodDeal(filter: $filter, pageable: { number: $number, size: $size}, orders: $orders) { content {" +
                            "sellerName sellerInn buyerName buyerInn woodVolumeBuyer woodVolumeSeller dealDate dealNumber __typename } __typename } }",
                Size = size,
                Number = number,
                Orders = null,
            });

            if (_request != null)
            {
                var response = _request.PostRequest(postDataBytes);
                var data = JsonConvert.DeserializeObject<Root3>(response);
                return data.data.searchReportWoodDeal.content;
            }
            else
                throw new ApplicationException("Request is null");
        }
    }
    public class ReportWoodDeal
    {
        public string sellerName { get; set; }
        public string sellerInn { get; set; }
        public string buyerName { get; set; }
        public string buyerInn { get; set; }
        public double woodVolumeBuyer { get; set; }
        public double woodVolumeSeller { get; set; }
        public string dealDate { get; set; }
        public string dealNumber { get; set; }
    }

    public class SearchReportWoodDeal2
    {
        [JsonProperty("Content")]
        public List<ReportWoodDeal> content { get; set; }
    }

    public class Data2
    {
        public SearchReportWoodDeal2 searchReportWoodDeal { get; set; }
    }

    public class Root3
    {
        public Data2 data { get; set; }
    }
}


